import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DominoTest {

    @Test
    public void testPlace() {
        Domino domino = new Domino(4, 2);
        domino.place(2, 3, 1, 4);

        assertTrue(domino.placed);
        assertEquals(2, domino.hx);
        assertEquals(3, domino.hy);
        assertEquals(1, domino.lx);
        assertEquals(4, domino.ly);
    }

    @Test
    public void testToStringUnplaced() {
        Domino domino = new Domino(4, 2);

        assertEquals("[42]unplaced", domino.toString());
    }

    @Test
    public void testToStringPlaced() {
        Domino domino = new Domino(4, 2);
        domino.place(2, 3, 1, 4);

        assertEquals("[42](3,4)(2,1)", domino.toString());
    }

    @Test
    public void testInvert() {
        Domino domino = new Domino(4, 2);
        domino.place(2, 3, 1, 4);

        domino.invert();

        assertEquals(1, domino.hx);
        assertEquals(4, domino.hy);
        assertEquals(2, domino.lx);
        assertEquals(3, domino.ly);
    }

    @Test
    public void testIshl() {
        Domino domino1 = new Domino(4, 2);
        domino1.place(2, 3, 1, 4);

        Domino domino2 = new Domino(1, 1);
        domino2.place(2, 2, 1, 1);

        assertFalse(domino1.ishl());
        assertTrue(domino2.ishl());
    }

    @Test
    public void testCompareTo() {
        Domino domino1 = new Domino(4, 2);
        Domino domino2 = new Domino(3, 1);
        Domino domino3 = new Domino(2, 4);

        assertTrue(domino1.compareTo(domino2) < 0);
        assertTrue(domino1.compareTo(domino3) > 0);
        assertEquals(0, domino1.compareTo(domino1));
    }
}
