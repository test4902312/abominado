import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class IOLibraryTest {

    @Test
    public void testGetString() throws IOException {
        String input = "Hello, World!";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try (MockedStatic<IOLibrary> mockedStatic = mockStatic(IOLibrary.class)) {
            mockedStatic.when(IOLibrary::getReader).thenReturn(reader);

            String result = IOLibrary.getString();

            assertEquals(input, result);
        }
    }

    @Test
    public void testGetIPAddress() throws IOException {
        String input = "192.168.0.1";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try (MockedStatic<IOLibrary> mockedStatic = mockStatic(IOLibrary.class)) {
            mockedStatic.when(IOLibrary::getReader).thenReturn(reader);

            InetAddress result = IOLibrary.getIPAddress();

            assertEquals("192.168.0.1", result.getHostAddress());
        }
    }
}
