import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiLinugualStringTableTest {

    @Test
    public void testGetMessageEnglish() {
        MultiLinugualStringTable.setLanguageSetting(MultiLinugualStringTable.LanguageSetting.English);

        assertEquals("Enter your name:", MultiLinugualStringTable.getMessage(0));
        assertEquals("Welcome", MultiLinugualStringTable.getMessage(1));
        assertEquals("Have a good time playing Abominodo", MultiLinugualStringTable.getMessage(2));
    }

    @Test
    public void testGetMessageKlingon() {
        MultiLinugualStringTable.setLanguageSetting(MultiLinugualStringTable.LanguageSetting.Klingon);

        assertEquals("'el lIj pong:", MultiLinugualStringTable.getMessage(0));
        assertEquals("nuqneH", MultiLinugualStringTable.getMessage(1));
        assertEquals("QaQ poH Abominodo", MultiLinugualStringTable.getMessage(2));
    }
}
