import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.mockito.MockedStatic;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ConnectionGeniusTest {

    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream originalOutput = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outputStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOutput);
    }

    @Test
    public void testInitializeGame() {
        InetAddress mockAddress = mock(InetAddress.class);
        ConnectionGenius genius = new ConnectionGenius(mockAddress);

        try (MockedStatic<ConnectionGenius> mockedStatic = mockStatic(ConnectionGenius.class)) {
            mockedStatic.when(() -> ConnectionGenius.downloadWebVersion()).thenCallRealMethod();
            mockedStatic.when(() -> ConnectionGenius.connectToWebService()).thenCallRealMethod();
            mockedStatic.when(() -> ConnectionGenius.startGame()).thenCallRealMethod();

            genius.initializeGame();

            String expectedOutput = "Getting specialised web version.\n" +
                    "Wait a couple of moments\n" +
                    "Connecting\n" +
                    "Ready to play\n";
            assertEquals(expectedOutput, outputStream.toString());
        }
    }
}
