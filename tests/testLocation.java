import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LocationTest {

    @Test
    public void testConstructorWithRowAndColumn() {
        Location location = new Location(2, 3);

        assertEquals(2, location.row);
        assertEquals(3, location.column);
        assertNull(location.direction);
    }

    @Test
    public void testConstructorWithRowColumnAndDirection() {
        Location location = new Location(2, 3, Location.DIRECTION.VERTICAL);

        assertEquals(2, location.row);
        assertEquals(3, location.column);
        assertEquals(Location.DIRECTION.VERTICAL, location.direction);
    }

    @Test
    public void testToStringWithoutDirection() {
        Location location = new Location(2, 3);

        assertEquals("(4,3)", location.toString());
    }

    @Test
    public void testToStringWithDirection() {
        Location location = new Location(2, 3, Location.DIRECTION.HORIZONTAL);

        assertEquals("(4,3,HORIZONTAL)", location.toString());
    }
}
