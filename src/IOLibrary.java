import java.io.*;
import java.net.*;

public final class IOLibrary {
   static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
  public static String getString() {
    do {
      try {
        return reader.readLine();
      } catch (Exception e) {
        e.printStackTrace();
      }
    } while (true);
  }

  public static InetAddress getIPAddress() {
    do {
      try {
        String[] chunks = reader.readLine().split("\\.");
        byte[] data = { Byte.parseByte(chunks[0]),Byte.parseByte(chunks[1]),Byte.parseByte(chunks[2]),Byte.parseByte(chunks[3])};
        return Inet4Address.getByAddress(data);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } while (true);
  }

}
