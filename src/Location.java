
public class Location extends SpacePlace {
  public int column;
  public int row;
  public DIRECTION direction;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int row, int column) {
    this.row = row;
    this.column = column;
  }

  public Location(int row, int column, DIRECTION direction) {
    this(row, column);
    this.direction = direction;
  }
  
  public String toString() {
    if(direction ==null){
      return "(" + (column +1) + "," + (row +1) + ")";
    } else {
      return "(" + (column +1) + "," + (row +1) + "," + direction + ")";
    }
  }
}
