public class DominoPlaceCommand implements GameCommand{

    private Domino domino;
    public int hx;
    public int hy;
    public int lx;
    public int ly;

    DominoPlaceCommand(Domino domino, int hx, int hy, int lx, int ly){
        this.domino = domino;
        this.hx = hx;
        this.hy = hy;
        this.lx = lx;
        this.ly = ly;
    }
    @Override
    public void execute() {
        domino.place(domino.hx, domino.hy , domino.lx,domino.ly);
        domino.placed = true;
    }

    @Override
    public void undo() {
        domino.placed = false;
        this.hx = 9;
        this.hy = 9;
        this.lx = 9;
        this.ly = 9;
    }
}
