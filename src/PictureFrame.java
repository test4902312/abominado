import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  public int[] reroll = null;
  Aardvark master = null;

  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;
    private static final int GRID_SIZE = 20;
    private static final int GRID_OFFSET = 20;
    private static final int DIAMETER = 20;
    private static final int NUM_ROWS = 7;
    private static final int NUM_COLS = 8;

    public void drawGrid(Graphics g) {
      for (int row = 0; row < NUM_ROWS; row++) {
        for (int col = 0; col < NUM_COLS; col++) {
          drawDigitGivenCentre(g, 30 + col * GRID_SIZE, 30 + row * GRID_SIZE, DIAMETER, master.grid[row][col]);
        }
      }
    }

    public void drawGridLines(Graphics g) {
      g.setColor(Color.LIGHT_GRAY);
      for (int row = 0; row <= NUM_ROWS; row++) {
        int y1RowCoordinate = GRID_OFFSET + row * GRID_SIZE;
        int x2RowCoordinate = GRID_OFFSET + NUM_COLS * GRID_SIZE;
        int y2RowCoordinate = GRID_OFFSET + row * GRID_SIZE;
        g.drawLine(GRID_OFFSET, y1RowCoordinate, x2RowCoordinate , y2RowCoordinate);
      }
      for (int col = 0; col <= NUM_COLS; col++) {
        int x1ColCoordinate = GRID_OFFSET + col * GRID_SIZE;
        int x2ColCoordinate = GRID_OFFSET + NUM_COLS * GRID_SIZE;
        int y2ColCoordinate = GRID_OFFSET + col * GRID_SIZE;
        g.drawLine(x1ColCoordinate, GRID_OFFSET, x2ColCoordinate, y2ColCoordinate);
      }
    }

    public void drawHeadings(Graphics g) {
      for (int row = 0; row < NUM_ROWS; row++) {
        fillDigitGivenCentre(g, 10, 30 + row * GRID_SIZE, DIAMETER, row + 1);
      }

      for (int col = 0; col < NUM_COLS; col++) {
        fillDigitGivenCentre(g, 30 + col * GRID_SIZE, 10, DIAMETER, col + 1);
      }
    }

    public void drawDomino(Graphics g, Domino d) {
      if (d.placed) {
        int y = Math.min(d.ly, d.hy);
        int x = Math.min(d.lx, d.hx);
        int w = Math.abs(d.lx - d.hx) + 1;
        int h = Math.abs(d.ly - d.hy) + 1;
        g.setColor(Color.WHITE);
        g.fillRect(GRID_OFFSET + x * GRID_SIZE, GRID_OFFSET + y * GRID_SIZE, w * GRID_SIZE, h * GRID_SIZE);
        g.setColor(Color.RED);
        g.drawRect(GRID_OFFSET + x * GRID_SIZE, GRID_OFFSET + y * GRID_SIZE, w * GRID_SIZE, h * GRID_SIZE);
        drawDigitGivenCentre(g, 30 + d.hx * GRID_SIZE, 30 + d.hy * GRID_SIZE, DIAMETER, d.high);
        drawDigitGivenCentre(g, 30 + d.lx * GRID_SIZE, 30 + d.ly * GRID_SIZE, DIAMETER, d.low);
      }
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.BLACK);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }
    
    void fillDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.GREEN);
      g.fillOval(x - radius, y - radius, diameter, diameter);
      g.setColor(Color.BLACK);
      g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());

      if (master.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawGuesses(g);
      }
      if (master.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawDominoes(g);
      }
    }

    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public DominoPanel dp;

  public void PictureFrame(Aardvark sf) {
    master = sf;
    if (dp == null) {
      JFrame f = new JFrame("Abominodo");
      dp = new DominoPanel();
      f.setContentPane(dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // Reset the reroll array
//    reroll = null;
//
//    // Reset the master object
//    if (master != null) {
//      master.reset();
//    }
//
//    // Repaint the panel
//    if (dp != null) {
//      dp.repaint();
//    }

  }

}
