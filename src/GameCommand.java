public interface GameCommand {
    void execute();
    void undo();
}
